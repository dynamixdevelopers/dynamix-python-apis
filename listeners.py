#
# Copyright (C) The Ambient Dynamix Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


class BindListener(object):
    def __init__(self):
        pass

    def on_bind(self):
        pass

    def on_bind_error(self, error_message):
        pass

    def on_unbind(self):
        pass


class SessionListener(object):
    def __init__(self):
        pass

    def on_session_opened(self, session_id):
        pass

    def on_session_closed(self):
        pass

    def on_dynamix_framework_active(self):
        pass

    def on_dynamix_framework_inactive(self):
        pass

    def on_context_plugin_discovery_started(self):
        pass

    def on_context_plugin_discovery_finished(self):
        pass

    def on_context_plugin_installed(self, context_plugin):
        pass

    def on_context_plugin_enabled(self, context_plugin):
        pass

    def on_context_plugin_disabled(self, context_plugin):
        pass

    def on_context_plugin_uninstalled(self, context_plugin):
        pass

    def on_context_plugin_error(self, context_plugin, error_message, error_code):
        pass


class ContextSupportListener(object):
    def __init__(self):
        pass

    def on_context_listener_removed(self):
        pass

    def on_context_result(self, context_result):
        pass

    def on_context_support_removed(self, support_info, error_message, error_code):
        pass




