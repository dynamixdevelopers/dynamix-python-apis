#
# Copyright (C) The Ambient Dynamix Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


class Callback(object):
    def on_success(self):
        pass

    def on_failure(self, error_message, error_code):
        pass


class SessionCallback(object):
    def on_success(self):
        pass

    def on_failure(self, error_message, error_code):
        pass


class HandlerCallback(object):
    def on_success(self, handler):
        pass

    def on_failure(self, error_message, error_code):
        pass


class ContextSupportCallback(object):
    def on_success(self, context_support_info):
        pass

    def on_failure(self, error_message, error_code):
        pass

    def on_warning(self, warning_message, warning_code):
        pass

    def on_progress(self, progress):
        pass


class ContextRequestCallback(object):
    def on_success(self, context_result):
        pass

    def on_failure(self, error_message, error_code):
        pass