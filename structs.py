#
# Copyright (C) The Ambient Dynamix Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


class ContextPluginInformation(object):
    def __init__(self, plugin_id, plugin_name, plugin_description, version_info, supported_context_types,
                 install_status, plugin_type, requires_configuration, is_configured, is_enabled, is_background_service,
                 extras, dynamix_features, repository_info):
        self.__plugin_id = plugin_id
        self.__plugin_name = plugin_name
        self.__plugin_description = plugin_description
        self.__version_info = version_info
        self.__supported_context_types = supported_context_types
        self.__install_status = install_status
        self.__plugin_type = plugin_type
        self.__requires_configuration = requires_configuration
        self.__is_configured = is_configured
        self.__is_enabled = is_enabled
        self.__is_background_service = is_background_service
        self.__extras = extras
        self.__dynamix_features = dynamix_features
        self.__repository_info = repository_info

    def get_plugin_id(self):
        return self.__plugin_id

    def get_plugin_name(self):
        return self.__plugin_name

    def get_plugin_description(self):
        return self.__plugin_description

    def get_version(self):
        return self.__version_info

    def get_supported_context_types(self):
        return self.__supported_context_types

    def get_context_plugin_type(self):
        return self.__plugin_type

    def get_requires_configuration(self):
        return self.__requires_configuration

    def get_is_configured(self):
        return self.__is_configured

    def is_enabled(self):
        return self.__is_enabled

    def is_background_service(self):
        return self.__is_background_service

    def get_extras(self):
        return self.__extras

    def get_dynamix_features(self):
        return self.__dynamix_features

    def get_repository_info(self):
        return self.__repository_info


class VersionInfo(object):
    def __init__(self, value, major, minor, micro, build, qualifier=None):
        self.value = value
        self.major = major
        self.minor = minor
        self.micro = micro
        self.build = build
        if qualifier is not None:
            self.qualifier = qualifier

    def get_value(self):
        return self.value

    def get_major(self):
        return self.major

    def get_minor(self):
        return self.minor

    def get_micro(self):
        return self.micro

    def get_build(self):
        return self.build

    def get_qualifier(self):
        return self.qualifier


class ContextType(object):
    def __init__(self, id, name, description):
        self.id = id
        self.name = name
        self.description = description

    def get_id(self):
        return self.id

    def get_name(self):
        return self.name

    def get_description(self):
        return self.description


class DynamixFeature(object):
    def __init__(self, plug_id, version_info, name, description, runtime_method, categories=None):
        self.plug_id = plug_id
        self.version_info = version_info
        self.name = name
        self.description = description
        self.runtime_method = runtime_method
        if categories is not None:
            self.categories = categories

    def get_plug_id(self):
        return self.plug_id

    def get_version_info(self):
        return self.version_info

    def get_name(self):
        return self.name

    def get_description(self):
        return self.description

    def get_runtime_method(self):
        return self.runtime_method

    def get_categories(self):
        return self.categories


class RepositoryInfo(object):
    def __init__(self, id, alias, url, type, enabled, dynamix_repo):
        self.id = id
        self.alias = alias
        self.url = url
        self.type = type
        self.enabled = enabled
        self.dynamix_repo = dynamix_repo

    def get_id(self):
        return self.id

    def get_alias(self):
        return self.alias

    def get_url(self):
        return self.alias

    def get_type(self):
        return self.type

    def is_enabled(self):
        return self.enabled

    def is_dynamix_repo(self):
        return self.dynamix_repo


class ContextSupportInfo(object):
    def __init__(self, support_id, plugins, context_type):
        self.support_id = support_id
        self.plugins = plugins
        self.contextType = context_type

    def get_support_id(self):
        return self.support_id

    def get_plugins(self):
        return self.plugins

    def get_context_type(self):
        return self.contextType


class ContextResult:
    def __init__(self, **entries):
        self.__dict__.update(entries)


