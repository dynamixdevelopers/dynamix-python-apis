#
# Copyright (C) The Ambient Dynamix Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from structs import DynamixFeature, ContextPluginInformation, ContextType, RepositoryInfo, VersionInfo, ContextSupportInfo


class DynamixParser:
    @staticmethod
    def get_context_plugin_info_list(plugins_list):
        context_plugins = []
        for p in plugins_list:
            context_plugin_information = DynamixParser.get_context_plugin_info_object(p)
            context_plugins.append(context_plugin_information)
        return context_plugins

    @staticmethod
    def get_context_plugin_info_object(p):
        plugin_version_info = DynamixParser.get_version_info_object(p["version"])

        supported_context_types = []
        for c in p["supportedContextTypes"]:
            supported_context_types.append(c)

        dynamix_features = []
        for f in p["dynamixFeatures"]:
            dynamix_features.append(f)

        repository_info = DynamixParser.get_repository_info_object(p["repository"])

        context_plugin_information = ContextPluginInformation(p["pluginId"], p["pluginName"],
                                                              p["pluginDescription"],
                                                              plugin_version_info, supported_context_types,
                                                              p["installStatus"], p["contextPluginType"],
                                                              p["requiresConfiguration"], p["configured"],
                                                              p["enabled"], p["backgroundService"],
                                                              p["extras"],
                                                              dynamix_features, repository_info)
        return context_plugin_information

    @staticmethod
    def get_context_type_object(c):
        context_type = ContextType(c["id"], c["name"], c["description"])
        return context_type

    @staticmethod
    def get_version_info_object(v):
        plugin_version_info = VersionInfo(v["value"], v["major"], v["minor"], v["micro"], v["build"],
                                          v["qualifier"])
        return plugin_version_info

    @staticmethod
    def get_dynamix_feature_object(f):
        v = f["pluginVersion"]
        version_info = DynamixParser.get_version_info_object(v)
        dynamix_feature = DynamixFeature(f["plugId"], version_info, f["name"],
                                         f["description"],
                                         f["runtimeMethod"], f["categories"])
        return dynamix_feature

    @staticmethod
    def get_repository_info_object(r):
        repository_info = RepositoryInfo(r["id"], r["alias"], r["url"], r["type"], r["enabled"], r["dynamixRepo"])
        return repository_info

    @staticmethod
    def get_context_support_info_object(s):
        context_plugins = DynamixParser.get_context_plugin_info_list(s["plugins"])
        context_support_info = ContextSupportInfo(s["supportId"], context_plugins, s["contextType"])
        return context_support_info