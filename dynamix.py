#
# Copyright (C) The Ambient Dynamix Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import urllib
import time
import uuid
import json
import os
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Cipher import AES
import base64

from parser import DynamixParser
from structs import ContextResult
import unirest
from zeroconf.zeroconf import *
import socket
import time

class Dynamix:
    binding = False
    bound = False
    http_token = None
    listeners = {}
    callbacks = {}
    bind_listener = None
    ip_address = None
    port = None
    instance_id = None

    def __init__(self):
        pass

    @staticmethod
    def getEncryptedDynamixUrl(endpointParamsString):
	url = "http://" + Dynamix.ip_address + ":" + Dynamix.port + "/"
        endpointParamsString = EncryptionParams.aesMasterCipher.encrypt(endpointParamsString, EncryptionParams.master_key)
        url += endpointParamsString
        return url
		
    @staticmethod
    def bind(bind_listener=None):
        #print "Starting to bind"
        if bind_listener is not None:
            Dynamix.bind_listener = bind_listener

        def bind_helper():
            print "Bind Helper"
            if Dynamix.binding:
                unirest.timeout(2)

                def request_callback(response):
                    if response.code == 200:
                        print "Looks like we are bound!!"
                        decrypted_bind_response = EncryptionUtils.decryptRSA(response.body)
                        #print "Decrypted response", decrypted_bind_response
                        response_object = json.loads(urllib.unquote(decrypted_bind_response))
                        EncryptionParams.master_key = base64.b64decode(response_object["masterKey"]).encode("hex")
                        #print "MasterKey : ", EncryptionParams.master_key
                        EncryptionParams.aesMasterCipher = AESCipher(EncryptionParams.master_key)
                        rnc = response_object["rnc"]
                        #print "Received Nonce :", rnc
                        if EncryptionParams.binding_nonce == rnc:
                            Dynamix.bound = True
                            # Setting timeout for 12 seconds. Dynamix on Android uses 10 seconds.
                            # This is a global setting and will be used for all requests from now on
                            unirest.timeout(12)
                            Dynamix.__event_loop()
                            # Notify Bind Success
                            DynamixListener.onDynamixFrameworkBound()
                        else :
                            print "Nonce doesnt match. Possible Hack!!"
                        return
                    elif response.code == 400:
                        Dynamix.binding = False
                        # Notify Bind Error
                        DynamixListener.onDynamixFrameworkBindError("Bad HTTP Request")
                        return
                    elif response.code == 403:
                        #print "Authorization error during bind"
                        Dynamix.binding = False
                        # Notify Bind Error
                        DynamixListener.onDynamixFrameworkBindError("Authorization error during bind")
                        return
                    else:
                        #print "Failed to bind. Please ensure that the device is on the same WiFi network."
                        #print response.code
                        #print response.body
                        Dynamix.binding = False
                        # Notify bind listeners
                        DynamixListener.onDynamixFrameworkBindError("Failed to bind to Dynamix!!")
                        return

                try:
                    rnc = PairingUtils.generate_12_digit_str()
                    EncryptionParams.binding_nonce = rnc
                    sha256hash = SHA256.new()
                    sha256hash.update(EncryptionParams.init_key + rnc)
                    signature_hex_string= sha256hash.hexdigest()
                    signature = "signature=" + signature_hex_string + "&rnc=" + rnc
                    if EncryptionParams.web_agent_pvtkey_pem is None or EncryptionParams.web_agent_pubkey_pem is None :
                        #print "Generating RSA Key pair for Web Agent"
                        EncryptionParams.web_agent_pvtkey_pem, EncryptionParams.web_agent_pubkey_pem, = EncryptionUtils.generate_RSA(
                            bits=2048)
                    rsa_encoded_signature = EncryptionUtils.doRSAFromBytes(EncryptionParams.adf_pub_key, signature)   
                    unirest.get("http://" + Dynamix.ip_address + ":" + Dynamix.port + "/dynamixBind",
                                params={"data": urllib.quote(rsa_encoded_signature, ''),
                                        "tempClientPublicKey": urllib.quote(EncryptionParams.web_agent_pubkey_pem, '')},
                                callback=request_callback)
                except Exception, err:
                    print "Exception in Bind Request : ", err
                    pass

        if os.path.isfile('cookies.json'):
            #print "Cookies file exists! Loading data from file."
            with open('cookies.json') as cookies:
                data = json.load(cookies)
                salt = data["salt"].decode("hex")
                #print "Salt : ", salt
                key = PBKDF2(PairingParams.pairing_password, salt)
                #print "Generated key from password : ", key 
                cipher = AESCipher(key.encode("hex"))
                Dynamix.instance_id = cipher.decrypt(data["dynamixInstanceId"])
                #print "Decrypted instance id :", Dynamix.instance_id
                EncryptionParams.init_key = cipher.decrypt(data["initKey"])
                #print "Decrypted init key", EncryptionParams.init_key
                Dynamix.http_token = cipher.decrypt(data["httpToken"])
                # Setting the authorization header once.
                # We will not need to do this on every request from now on.
                unirest.default_header('httptoken', Dynamix.http_token)
                #print "Decrypted http token : ", Dynamix.http_token
                if Dynamix.ip_address is not None and Dynamix.port is not None:
                    Dynamix.binding = True
                    bind_helper()
                else:
                    def callback(response):
                        #print "Bind Request response code ", response.code
                        if response.code == 200:
                            Dynamix.ip_address = response.body["instanceIp"]
                            #print "Dynamix IP Address : ", Dynamix.ip_address
                            Dynamix.port = response.body["instancePort"]
                            #print "Dynamix Port : ", Dynamix.port
                            EncryptionParams.adf_pub_key = response.body["instancePublicKey"]
                            #print "Dynamix Public Key : ", EncryptionParams.adf_pub_key
                            Dynamix.binding = True
                            bind_helper()

                    unirest.get(PairingParams.pairing_server_address +
                                "getInstanceFromId.php", params={"instanceId": Dynamix.instance_id},
                                callback=callback)
        else:
            #print "No cookies file found, initiating pairing"
            Dynamix.binding = False
            DynamixPairing.write_pairing_code_to_nfc()


    @staticmethod
    def __event_loop():
        if Dynamix.bound:
            def request_callback(response):
                if response.code == 200:
                    result = urllib.unquote_plus(EncryptionParams.aesMasterCipher.decrypt(response.body))
                    #print result
                    result_obj = json.loads(result)
                    callback_id = result_obj["callbackId"]
                    method_name = result_obj["method"]
                    #print callback_id, method_name
                    params = result_obj["params"]
                    method_to_call = getattr(DynamixListener, method_name)
                    method_to_call(callback_id, params)
                    time.sleep(0.25)
                    Dynamix.__event_loop()
                elif response.code == 404:
                    time.sleep(0.25)
                    Dynamix.__event_loop()
                elif response.code == 403:
                    print "Event loop error : invalid token"
                elif response.code == 503:
                    print "Event loop error : service unavailable"
                elif response.code == 400:
                    print "Event loop error : bad request"
                elif response.code == -1:
                    #print response.body
                    #print 'Event loop stopped. Dynamix has probably moved out of the network. Should start service discovery here.'
                    if Dynamix.bound :
                        #Set bound status to false and start service discovery.
                        Dynamix.bound = False
                        class MyListener(object):
                            def __init__(self):
                                self.r = Zeroconf()

                            def removeService(self, zeroconf, type, name):
                                print
                                print "Service", name, "removed"

                            def addService(self, zeroconf, type, name):
                                #print
                                #print "Service", name, "added"
                                #print "  Type is", type
                                info = self.r.getServiceInfo(type, name)
                                if info:
                                    print "  Address is %s:%d" % (socket.inet_ntoa(info.getAddress()),
                                                                  info.getPort())
                                    print "  Weight is %d, Priority is %d" % (info.getWeight(),
                                                                              info.getPriority())
                                    print "  Server is", info.getServer()
                                    prop = info.getProperties()
                                    if prop:
                                        #print "  Properties are"
                                        dynamixData = prop.items()
                                        for key, value in prop.items():
                                            #print "    %s: %s" % (key, value)
                                            if key == "instanceId":
                                                #print "Instance Id : ", value
                                                if value == Dynamix.instance_id:
                                                    #print "value matches our instance Id, making bind call"
                                                    Dynamix.bind()
                                                    self.r.close()
                                    else:
                                        print "could not fetch dynamix service properties"
                                    
                        r = Zeroconf()
                        print "Starting dynamix service discovery"
                        type = "_dynamix._tcp.local."
                        listener = MyListener()
                        browser = ServiceBrowser(r, type, listener)
                else:
                    print "request_callback eventLoop : " + str(response.code)

            try:
                #print "New /eventcallback request"
                endpointParamsString = "eventcallback"
                endpointParamsString = EncryptionParams.aesMasterCipher.encrypt(endpointParamsString, EncryptionParams.master_key)
                unirest.get("http://" + Dynamix.ip_address + ":" + Dynamix.port + "/" + endpointParamsString, callback=request_callback)
            except Exception, err:
                print "Exception in event loop", err


    @staticmethod
    def open_dynamix_session(session_listener=None, callback=None):
        endpointParamsString = "opendynamixsession?param=1"
        if session_listener is not None:
            listener_id = str(uuid.uuid1())
            Dynamix.listeners[listener_id] = session_listener
            endpointParamsString += "&sessionListenerId=" + listener_id
        if callback is not None:
            callback_id = str(uuid.uuid1())
            Dynamix.callbacks[callback_id] = callback
            endpointParamsString += "&callbackId=" + callback_id
        try:
            url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
            unirest.get(url, callback=None)
        except Exception, err:
            #print "Exception in openDynamixSession",err
            pass


    @staticmethod
    def create_context_handler(callback):
        callback_id = str(uuid.uuid1())
        Dynamix.callbacks[callback_id] = callback
        endpointParamsString = "createcontexthandler?callbackId=" + callback_id			
        url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
        unirest.get(url, callback = None)


    @staticmethod
    def set_dynamix_session_listener(session_listener):
        session_listener_id = str(uuid.uuid1())
        Dynamix.listeners[session_listener_id] = session_listener
        endpointParamsString = "setdynamixsessionlistener?sessionListenerId=" + session_listener_id
	url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
        unirest.get(url, callback = None)


    @staticmethod
    def get_dynamix_version():
        r = unirest.get(Dynamix.base_url + ":" + Dynamix.port + "/dynamixVersion")
        return r.body

    @staticmethod
    def remove_context_handler(handler, callback = None):
	endpointParamsString = "removecontexthandler?contextHandlerId=" + handler.handler_id
        if callback is not None:
            callback_id = str(uuid.uuid1())
            Dynamix.callbacks[callback_id] = callback
            endpointParamsString += "&callbackId=" + callback_id
	url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
        unirest.get(url, callback=None)


    @staticmethod
    def close_dynamix_session(callback = None):
        endpointParamsString = "closedynamixsession"
        if callback is not None:
            callback_id = str(uuid.uuid1())
            Dynamix.callbacks[callback_id] = callback
            endpointParamsString += "?callbackId=" + callback_id
        url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
        unirest.get(url, callback=None)


    class Handler:
        def __init__(self, handler_id):
            self.handler_id = handler_id

        def add_context_support(self, plugin_id, context_type, callback=None, listener=None, plugin_version=None):
            endpointParamsString = "addContextSupport?contextHandlerId=" + self.handler_id + "&pluginId=" + plugin_id + "&contextType=" + context_type
            if callback is not None:
                callback_id = str(uuid.uuid1())
                Dynamix.callbacks[callback_id] = callback
                endpointParamsString += "&callbackId=" + callback_id
            if listener is not None:
                listener_id = str(uuid.uuid1())
                Dynamix.listeners[listener_id] = listener
                endpointParamsString += "&contextListenerId=" + listener_id
            if plugin_version is not None:
                endpointParamsString += "&pluginVersion=" + str(plugin_version)
	    url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
            unirest.get(url, callback=None)

        def context_request(self, plugin_id, context_type, callback, plugin_version=None):
            callback_id = str(uuid.uuid1())
            Dynamix.callbacks[callback_id] = callback
            endpointParamsString = "contextRequest?contextHandlerId=" + self.handler_id + "&pluginId=" + plugin_id + "&contextType=" + context_type
            if plugin_version is not None:
                endpointParamsString += "&pluginVersion=" + str(plugin_version)
	    url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
            unirest.get(url, callback=None)

        def __make_configured_request(self, endpoint, method, plugin_id, context_type, callback=None,
                                      plugin_version=None,
                                      params=None, headers=None):
            if params is None:
                params = {}
            if headers is None:
                headers = {}
            method = method.upper()
            endpointParamsString = endpoint + "?contextHandlerId" + self.handler_id + "&pluginId=" + plugin_id \
                  + "&contextType=" + context_type
            if callback is not None:
                callback_id = str(uuid.uuid1())
                Dynamix.callbacks[callback_id] = callback
                endpointParamsString += "&callbackId=" + callback_id
            if plugin_version is not None:
                endpointParamsString += "pluginVersion=" + plugin_version    
	    url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
	    
	    ##  Need to check how this'll work in case of encryption
            if method == "GET":
                unirest.get(url, params=params, headers=headers, callback=None)
            elif method == "POST":
                unirest.post(url, params=json.dumps(params), headers=headers, callback=None)
            elif method == "PUT":
                unirest.put(url, params=json.dumps(params), headers=headers, callback=None)
            elif method == "DELETE":
                unirest.delete(url, params=json.dumps(params), headers=headers, callback=None)

        def configured_context_request(self, method, plugin_id, context_type, callback=None, plugin_version=None,
                                       params=None, headers=None):
            self.__make_configured_request("configuredcontextrequest", method, plugin_id, context_type,
                                           callback=callback, plugin_version=plugin_version, params=params,
                                           headers=headers)

        def add_configured_context_support(self, method, plugin_id, context_type, callback=None, plugin_version=None,
                                           params=None, headers=None):
            self.__make_configured_request("addconfiguredcontextsupport", method, plugin_id, context_type,
                                           callback=callback, plugin_version=plugin_version, params=params,
                                           headers=headers)

        def get_context_support_info(self, callback):
            endpointParamsString = "getContextSupport?contextHandlerId=" + self.handler_id
            if callback is not None:
		callback_id = str(uuid.uuid1())
                Dynamix.callbacks[callback_id] = callback
                endpointParamsString += "&callbackId=" + callback_id
            url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
            unirest.get(url, callback = None)

        def remove_context_support_for_context_type(self, context_type, callback=None):
            endpointParamsString = "removecontextsupportforcontexttype?contextHandlerId=" + self.handler_id + "&contextType=" + context_type
            if callback is not None:
                callback_id = str(uuid.uuid1())
                Dynamix.callbacks[callback_id] = callback
                endpointParamsString += "&callbackId=" + callback_id			
	    url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
            unirest.get(url, callback=None)

        def remove_context_support_for_support_id(self, support_id, callback=None):
            endpointParamsString = "removecontextsupportforsupportid?contextHandlerId=" + self.handler_id + "&supportId=" + support_id
            if callback is not None:
                callback_id = str(uuid.uuid1())
                Dynamix.callbacks[callback_id] = callback
                endpointParamsString += "&callbackId=" + callback_id
	    url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
            unirest.get(url, callback=None)

        def remove_all_context_support(self, callback=None):
            endpointParamsString = "removeallcontextsupport?contextHandlerId=" + self.handler_id
            if callback is not None:
                callback_id = str(uuid.uuid1())
                Dynamix.callbacks[callback_id] = callback
                endpointParamsString += "&callbackId=" + callback_id
	    url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
            unirest.get(url, callback=None)

        def open_context_plugin_configuration_view(self, plugin_version=None, callback = None):
            endpointParamsString =  "opencontextpluginconfigurationview?contextHandlerId=" + self.handler_id
	    if callback is not None:
                callback_id = str(uuid.uuid1())
                Dynamix.callbacks[callback_id] = callback
                endpointParamsString += "&callbackId=" + callback_id
            if plugin_version is not None:
                endpointParamsString += "&pluginVersion=" + plugin_version
	    url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
            unirest.get(url, callback=None)

        def open_default_context_plugin_configuration_view(self, plugin_version=None, callback = None):
            endpointParamsString =  "opendefaultcontextpluginconfigurationview?contextHandlerId=" + self.handler_id
	    if callback is not None:
                callback_id = str(uuid.uuid1())
                Dynamix.callbacks[callback_id] = callback
                endpointParamsString += "&callbackId=" + callback_id
            if plugin_version is not None:
                endpointParamsString += "&pluginVersion=" + plugin_version
	    url = Dynamix.getEncryptedDynamixUrl(endpointParamsString)
            unirest.get(url, callback=None)


class DynamixListener(object):
    @staticmethod
    def __init__():
        pass

    @staticmethod
    def onDynamixFrameworkBound():
        if hasattr(Dynamix.bind_listener, "on_bind"):
            Dynamix.bind_listener.on_bind()

    @staticmethod
    def onDynamixFrameworkUnbound():
        if hasattr(Dynamix.bind_listener, "on_unbind"):
            Dynamix.bind_listener.on_unbind()

    @staticmethod
    def onDynamixFrameworkBindError(error_message):
        if hasattr(Dynamix.bind_listener, "on_bind_error"):
            Dynamix.bind_listener.on_bind_error(error_message)

    # Web Session Callback methods
    @staticmethod
    def onSessionCallbackSuccess(callback_id, params):
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_success"):
            Dynamix.callbacks[callback_id].on_success()

    @staticmethod
    def onSessionCallbackFailure(callback_id, params):
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_failure"):
            Dynamix.callbacks[callback_id].on_failure(params["message"], params["errorCode"])

    # Web Session Listener methods
    @staticmethod
    def onSessionOpened(listener_id, params):
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_session_opened"):
            Dynamix.listeners[listener_id].on_session_opened(params["sessionId"])

    @staticmethod
    def onSessionClosed(listener_id, params):
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_session_closed"):
            Dynamix.listeners[listener_id].on_session_closed()

    @staticmethod
    def onDynamixFrameworkActive(listener_id, params):
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_dynamix_framework_active"):
            Dynamix.listeners[listener_id].on_dynamix_framework_active()

    @staticmethod
    def onDynamixFrameworkInactive(listener_id, params):
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id],
                                                        "on_dynamix_framework_inactive"):
            Dynamix.listeners[listener_id].on_dynamix_framework_inactive()

    @staticmethod
    def onContextPluginDiscoveryStarted(listener_id, params):
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id],
                                                        "on_context_plugin_discovery_started"):
            Dynamix.listeners[listener_id].on_context_plugin_discovery_started()

    @staticmethod
    def onContextPluginDiscoveryFinished(listener_id, params):
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id],
                                                        "on_context_plugin_discovery_finished"):
            Dynamix.listeners[listener_id].on_context_plugin_discovery_finished()

    @staticmethod
    def onContextPluginInstalled(listener_id, params):
        #print params
        context_plugin = DynamixParser.get_context_plugin_info_object(params["plugin"])
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_context_plugin_installed"):
            Dynamix.listeners[listener_id].on_context_plugin_installed(context_plugin)

    @staticmethod
    def onContextPluginUninstalled(listener_id, params):
        context_plugin = DynamixParser.get_context_plugin_info_object(params["plugin"])
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_context_plugin_installed"):
            Dynamix.listeners[listener_id].on_context_plugin_uninstalled(context_plugin)

    @staticmethod
    def onContextPluginError(listener_id, params):
        context_plugin = DynamixParser.get_context_plugin_info_object(params["plugin"])
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_context_plugin_error"):
            Dynamix.listeners[listener_id].on_context_plugin_error(context_plugin, params["message"],
                                                                   int(params["errorCode"]));

    @staticmethod
    def onContextPluginEnabled(listener_id, params):
        context_plugin = DynamixParser.get_context_plugin_info_object(params["plugin"])
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_context_plugin_enabled"):
            Dynamix.listeners[listener_id].on_context_plugin_enabled(context_plugin)

    @staticmethod
    def onContextPluginDisabled(listener_id, params):
        context_plugin = DynamixParser.get_context_plugin_info_object(params["plugin"])
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_context_plugin_disabled"):
            Dynamix.listeners[listener_id].on_context_plugin_disabled(context_plugin)

    # Context Support Callback methods
    @staticmethod
    def onContextSupportCallbackSuccess(callback_id, params):
        context_support_info = DynamixParser.get_context_support_info_object(params["supportInfo"])
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_success"):
            Dynamix.callbacks[callback_id].on_success(context_support_info)

    @staticmethod
    def onContextSupportCallbackProgress(callback_id, params):
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_progress"):
            Dynamix.callbacks[callback_id].on_progress(int(params["percentComplete"]))

    @staticmethod
    def onContextSupportCallbackWarning(callback_id, params):
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_warning"):
            Dynamix.callbacks[callback_id].on_warning(params["message"], int(params["errorCode"]))

    @staticmethod
    def onContextSupportCallbackFailure(callback_id, params):
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_failure"):
            Dynamix.callbacks[callback_id].on_failure(params["message"], int(params["errorCode"]))

    # Context Request Callback methods
    @staticmethod
    def onContextRequestCallbackSuccess(callback_id, params):
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_success"):
            Dynamix.callbacks[callback_id].on_success(ContextResult(**(params["contextResult"])))

    @staticmethod
    def onContextRequestCallbackFailure(callback_id, params):
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_failure"):
            Dynamix.callbacks[callback_id].on_failure(params["message"], int(params["errorCode"]))

    # Context Handler Callback methods
    @staticmethod
    def onContextHandlerCallbackSuccess(callback_id, params):
        #print "onContextHandlerCallbackSuccess", params
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_success"):
            Dynamix.callbacks[callback_id].on_success(
                Dynamix.Handler(handler_id=params["handlerId"]))

    @staticmethod
    def onContextHandlerCallbackFailure(callback_id, params):
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_failure"):
            Dynamix.callbacks[callback_id].on_failure(params["message"], int(params["errorCode"]))

    # Callback methods
    @staticmethod
    def onCallbackSuccess(callback_id, params):
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_success"):
            Dynamix.callbacks[callback_id].on_success()

    @staticmethod
    def onCallbackFailure(callback_id, params):
        if callback_id in Dynamix.callbacks and hasattr(Dynamix.callbacks[callback_id], "on_failure"):
            Dynamix.callbacks[callback_id].on_failure(params["message"], int(params["errorCode"]))

    # Context Listener Methods
    @staticmethod
    def onContextResult(listener_id, params):
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_context_result"):
            Dynamix.listeners[listener_id].on_context_result(ContextResult(**params))


    @staticmethod
    def onContextListenerRemoved(listener_id, params):
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_context_listener_removed"):
            Dynamix.listeners[listener_id].on_context_listener_removed()


    @staticmethod
    def onContextSupportRemoved(listener_id, params):
        context_support_info = DynamixParser.get_context_support_info_object(params["supportInfo"])
        if listener_id in Dynamix.listeners and hasattr(Dynamix.listeners[listener_id], "on_context_support_removed"):
            Dynamix.listeners[listener_id].on_context_support_removed(context_support_info, params["message"],
                                                                      int(params["errorCode"]))

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s: s[:-ord(s[len(s) - 1:])]


class AESCipher:
    def __init__(self, key):
        """
        Requires hex encoded param as a key
        """
        self.key = key.decode("hex")

    def encrypt(self, raw, iv):
        """
        Returns hex encoded encrypted value!
        """
        raw = pad(raw)
        iv = iv.decode("hex")
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode("hex")

    def decrypt(self, enc):
        """
        Requires hex encoded param to decrypt
        """
        enc = enc.decode("hex")
        cipher = AES.new(self.key, AES.MODE_CBC, self.key)
        return unpad(cipher.decrypt(enc))

class EncryptionParams:
    def __init__(self):
        pass

    adf_pub_key = None
    web_agent_pubkey_pem = None
    web_agent_pvtkey_pem = None
    init_key = None
    master_key = None
    binding_nonce = None
    aesMasterCipher = None


class EncryptionUtils:
    def __init__(self):
        pass

    @staticmethod
    def generate_RSA(bits=2048):
        new_key = RSA.generate(bits, e=65537)
        public_key = new_key.publickey().exportKey("PEM")
        private_key = new_key.exportKey("PEM")
        f = open("pubkey.pem", 'w')
        f.write(public_key)
        f.close()
        f = open("privatekey.pem", 'w')
        f.write(private_key)
        f.close()
        return private_key, public_key

    @staticmethod
    def doRSA(key_in_pem, plaintext):
        pubkey = RSA.importKey(key_in_pem)
        cipher = PKCS1_OAEP.new(pubkey, hashAlgo=SHA256)
        encrypted = cipher.encrypt(plaintext)
        return encrypted.encode('base64')

    @staticmethod
    def doRSAFromBytes(key, plaintext):
        #print "RSAEncrypting:"
        #print "PlainText : ", plaintext
        #print "Public Key : ", key
        key = key.replace('-----BEGIN PUBLIC KEY-----', '')
        key = key.replace('-----END PUBLIC KEY-----', '');
        #print "Removed headers in the key :"
        #print key
        pubkey = RSA.importKey(base64.b64decode(key))
        cipher = PKCS1_OAEP.new(pubkey, hashAlgo=SHA256)
        encrypted = cipher.encrypt(plaintext)
        #print encrypted
        #print type(encrypted)
        #print "Encrypted Data:"
        #print base64.b64encode(encrypted)
        # #print encrypted.encode('base64')
        return base64.b64encode(encrypted)

    @staticmethod
    def decryptRSA(ciphertext):
        rsa_private_key = RSA.importKey(open('privatekey.pem', 'r').read())
        cipher = PKCS1_OAEP.new(rsa_private_key, hashAlgo=SHA256)
        decrypted = cipher.decrypt(base64.b64decode(ciphertext))
        return decrypted

    
class PairingParams:
    pairing_server_address = "http://pairing.ambientdynamix.org/securePairing/"
    pair_code = None
    pairing_password = "WhatAnAmazingPassword!"

    def __init__(self):
        pass


class PairingUtils:
    def __init__(self):
        pass

    @staticmethod
    def generate_12_digit_str():
        import random

        return str(random.randint(10 ** 11, (10 ** 12) - 1))


class DynamixPairing:
    def __init__(self):
        pass


    @staticmethod
    def write_pairing_code_to_nfc():
	import subprocess
	import threading
        PairingParams.pair_code = PairingUtils.generate_12_digit_str()
	#print "New pairing code generated : " + PairingParams.pair_code
        sha256hash = SHA256.new()
        sha256hash.update(PairingParams.pair_code)
	def run_subprocess():
            #print "Writing to nfc"
            subprocess.call("./card_emulation.sh " + PairingParams.pair_code, shell=True)

        thread = threading.Thread(target=run_subprocess)
        thread.start()

        def request_callback(response):
            if response.code == 200:
                #print "Get Dynamix Instance Response Code : ", response.code
                #print "Get Dynamix Instance Response : ", response.body
                # Unirest : Parsed response body where applicable, for example JSON
                # #responses are parsed to Objects / Associative Arrays.
                response_object = response.body
                if "error" in response_object:
                    print "Error : ", response_object["error"]
                    get_instance_from_server(sha256hash.hexdigest(), request_callback)
                else:
                    Dynamix.ip_address = response_object["instanceIp"]
                    #print "Dynamix IP Address : ", Dynamix.ip_address
                    Dynamix.port = response_object["instancePort"]
                    #print "Dynamix Port : ", Dynamix.port
                    Dynamix.instance_id = response_object["instanceId"]
                    #print "Dynamix Instance ID : ", Dynamix.instance_id
                    EncryptionParams.adf_pub_key = response_object["instancePublicKey"]
                    #print "Dynamix Public Key : ", EncryptionParams.adf_pub_key
                    EncryptionParams.web_agent_pvtkey_pem, EncryptionParams.web_agent_pubkey_pem, = EncryptionUtils.generate_RSA(
                        bits=2048)
                    nonce = PairingUtils.generate_12_digit_str()
                    signature_string = "pairingCode=" + PairingParams.pair_code + "&rnc=" + nonce
                    signature = EncryptionUtils.doRSAFromBytes(EncryptionParams.adf_pub_key, signature_string)
                    pair_url = "http://" + Dynamix.ip_address + ":" + Dynamix.port + "/pair"
                    #print "Pair URL : ", pair_url

                    def pair_callback(pair_response):
                        if pair_response.code == 200:
                            try:
                                #print "Pair Response : ", pair_response.body
                                decrypted_response = EncryptionUtils.decryptRSA(pair_response.body)
                                decrypted_response = urllib.unquote(decrypted_response)
                                #print decrypted_response
                                pair_response_object = json.loads(decrypted_response)
                                if pair_response_object["rnc"] == nonce:
                                    #print "Yay!! Nonce matches!"
                                    EncryptionParams.init_key = pair_response_object["initKey"]
                                    Dynamix.http_token = pair_response_object["httpToken"]
                                    #print "New Http Token : ", Dynamix.http_token
                                    salt = os.urandom(8)
                                    #print "Salt : ", salt
                                    key = PBKDF2(PairingParams.pairing_password, salt) #Default key length is 16 bytes
                                    aesCipher = AESCipher(key.encode('hex'))
                                    iv = key.encode('hex')
                                    encrypted_init_key = aesCipher.encrypt(EncryptionParams.init_key, iv)
                                    #print "Encrypted initKey : ", encrypted_init_key
                                    encrypted_http_token = aesCipher.encrypt(Dynamix.http_token, iv)
                                    #print "Encrypted httpToken : ", encrypted_http_token
                                    encrypted_instance_id = aesCipher.encrypt(Dynamix.instance_id, iv)
                                    #print "Encrypted Instance Id : ", encrypted_instance_id
                                    with open('cookies.json', 'w') as outfile:
                                        json.dump({"dynamixInstanceId": encrypted_instance_id, "salt": salt.encode('hex'),
                                         "initKey": encrypted_init_key,
                                         "httpToken": encrypted_http_token}, outfile)
                                    Dynamix.bind()
                                else:
                                    print "Nonce doesn't match, possible hack!!!"
                            except Exception as ex:
                                print ex.message
                        else:
                            print "Pairing Response : ", pair_response.code, pair_response.body

                    unirest.get(pair_url,
                                params={"signature": urllib.quote(signature, ''),
                                        "tempClientPublicKey": urllib.quote(EncryptionParams.web_agent_pubkey_pem, '')},
                                callback=pair_callback)
            else:
                #print response.code
                get_instance_from_server(sha256hash.hexdigest(), request_callback)


        def get_instance_from_server(hash_code, callback):
            #print "Requesting instance for :", hash_code
            unirest.get(PairingParams.pairing_server_address + 'getDynamixInstance.php',
                        params={"hash": hash_code},
                        callback=callback)

        get_instance_from_server(sha256hash.hexdigest(), request_callback)


def __init__(self):
    pass


