#
# Copyright (C) The Ambient Dynamix Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from dynamix import Dynamix
from listeners import BindListener, SessionListener, ContextSupportListener
from callbacks import ContextSupportCallback, HandlerCallback, SessionCallback
import os

class MyBindListener(BindListener):
    def on_bind(self):
        print "Dynamix was bound! You can go ahead and try to open a session."

        class MySessionListener(SessionListener):
            def on_dynamix_framework_inactive(self):
                print "Framework InActive  "

            def on_dynamix_framework_active(self):
                print "Framework Active  "

            def on_session_opened(self, session_id):
                print "Yay!! Session was opened successfully with Dynamix ", session_id

            def on_context_plugin_disabled(self, params):
                print "Context Plugin Disabled : ", params

            def on_context_plugin_uninstalled(self, params):
                print "Context Plugin Uninstalled : ", params

            def on_context_plugin_enabled(self, params):
                print "Context Plugin Enabled : ", params

            def on_context_plugin_installed(self, params):
                print "Context Plugin Installed : ", params

            def on_context_plugin_error(self, context_plugin, error_message, error_code):
                print "Context Plugin Error : ", context_plugin, error_message, error_code

            def on_session_closed(self):
                print "Session Closed"

            def on_context_plugin_discovery_started(self):
                print "Plugin Discovery Started "

            def on_context_plugin_discovery_finished(self):
                print "Plugin Discovery Finished "

        class MySessionCallback(SessionCallback):
            def on_failure(self, error_message, error_code):
                print "Open dynamix session callback failure : ", error_message, error_code

            def on_success(self):
                print "Open dynamix session callback success : "

                class MyHandlerCallback(HandlerCallback):
                    def on_failure(self, error_message, error_code):
                        print "Failed to create a handler : ", error_message, error_code

                    def on_success(self, handler):
                        print "New handler received : " + str(handler.handler_id)
                        # Handler has been created, now we can make requests

                        class MyContextSupportCallback(ContextSupportCallback):
                            def on_success(self, support_info):
                                print "Context support callback success ", support_info

                            def on_warning(self, warning_message, warning_code):
                                print "Context support callback warning : ", warning_message, warning_code

                            def on_failure(self, error_message, error_code):
                                print "Context support callback failure : ", error_message, error_code

                            def on_progress(self, progress):
                                print "Context   support callback progress : ", progress

                        class MyContextSupportListener(ContextSupportListener):
                            def on_context_support_removed(self, context_support_info, error_message, error_code):
                                print "Context support removed : ", context_support_info, error_message, error_code

                            def on_context_listener_removed(self):
                                print "Context support listener removed : "

                            def on_context_result(self, params):
                                print "Context support listener result : ", params.sourcePluginId, params.direction
				if params.direction == "TOP":
					os.system("xscreensaver-command -l") 
				elif params.direction == "BOTTOM":
					os.system("xscreensaver-command -deactivate")
				
			'''
                        handler.add_context_support(plugin_id="org.ambientdynamix.contextplugins.batterylevel",
                                                    context_type="org.ambientdynamix.contextplugins.batterylevel",
                                                    callback=MyContextSupportCallback(),
                                                    listener=MyContextSupportListener())
			'''
			handler.add_context_support(plugin_id="org.ambientdynamix.contextplugins.gesture", 
							context_type="org.ambientdynamix.contextplugins.gesture.subscribe", 
							callback=MyContextSupportCallback(), 
							listener=MyContextSupportListener())			
                Dynamix.create_context_handler(callback=MyHandlerCallback())

        Dynamix.open_dynamix_session(session_listener=MySessionListener(), callback=MySessionCallback())

    def on_unbind(self):
        print "Dynamix unbound!!"

    def on_bind_error(self, error_message):
        print "Could not bind to Dynamix : ", error_message


myBindListener = MyBindListener()
Dynamix.bind(myBindListener)
