# Welcome
This wiki provides an overview of the python binding library for Ambient Dynamix. The library allows python apps to
easily request for contextual support from a Dynamix Device.

# Dependencies
* DynamixPy uses a modified version of [Unirest](http://unirest.io/python.html) for REST calls. Unirest
needs [poster >= 0.8.1](https://pypi.python.org/pypi/poster/) to run.

# Binding with Dynamix
* To initiate a connection with Dynamix, call the `bind()` method from the Dynamix class. The `bind()` method accepts a [listener](https://bitbucket.org/dynamixdevelopers/dynamix-python-apis/src/f6db8bed7514ad13378f0b2cc4dd0e9892171c70/listeners.py?at=encryption&fileviewer=file-view-default#listeners.py-17) which can listen to bind states. 
* `Dynamix.bind()`
* Dynamix Remote binding libraries support a host of pairing mechanisms. DynamixPy uses an NFC based pairing
mechanism. Once `bind()` is called, DynamixPy initiates the pairing by emulating a pairing
code on the Raspberry Pi NFC Chip. Any Dynamix device can then use the NFC to Interact feature from the 
[NFC To Interact](https://bitbucket.org/dynamixdevelopers/nfc-to-interact) plugin, to complete the pairing process. Just tap on the Raspberry Pi after starting the feature. 
* On successful pairing, `on_bind()` method is called on the bind listener. 

# Opening a Session
* After a successful binding, you can go ahead and open a session by calling the `open_dynamix_session()` method from the Dynamix class.
* `Dynamix.open_dynamix_session(session_listener=MySessionListener(), callback=MySessionCallback())`
* MySessionListener and MySessionCallback extend the
[SessionListener](https://bitbucket.org/dynamixdevelopers/dynamix-python-apis/src/f6db8bed7514ad13378f0b2cc4dd0e9892171c70/listeners.py?at=encryption&fileviewer=file-view-default#listeners.py-31)
and the [SessionCallback](https://bitbucket.org/dynamixdevelopers/dynamix-python-apis/src/f6db8bed7514ad13378f0b2cc4dd0e9892171c70/callbacks.py?at=encryption&fileviewer=file-view-default#callbacks.py-25) classes. 

# Creating a new Context Handler
* Once a session is successfully setup, you'll need a ContextHandler to start playing with context supports. The `create_context_handler()` method creates a new context handler. The new handler is returned in the `on_success()` method of the [HandlerCallback](https://bitbucket.org/dynamixdevelopers/dynamix-python-apis/src/f6db8bed7514ad13378f0b2cc4dd0e9892171c70/callbacks.py?at=develop&fileviewer=file-view-default#callbacks.py-33). 
* `Dynamix.create_context_handler(callback=MyHandlerCallback())`


# Adding context support
* The handler can be used to interact with all available Dyamix services such as adding context support, making context request etc. All supported methods are a part of the [Handler](https://bitbucket.org/dynamixdevelopers/dynamix-python-apis/src/bc49229500649bb8ea7010b8186fccb9e980cdea/dynamix.py?at=encryption&fileviewer=file-view-default#dynamix.py-318) class. 
* ` handler.add_context_support(plugin_id="org.ambientdynamix.contextplugins.batterylevel",
                                                    context_type="org.ambientdynamix.contextplugins.batterylevel",
                                                    callback=MyContextSupportCallback(),
                                                    listener=MyContextSupportListener())`
                                                    
#Listeners and Callbacks
* A full list of supported listeners and callbacks can be found in the listener.py and callbacks.py files respectively. 

# Sample Code
* The [dynamixlogger.py](https://bitbucket.org/dynamixdevelopers/dynamix-python-apis/src/bc49229500649bb8ea7010b8186fccb9e980cdea/dynamixlogger.py?at=encryption) script shows a full example of the whole process. It uses the Gesture Recognition Plugin to logout and login to the Raspberry Pi. 
